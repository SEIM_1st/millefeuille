/*
 * SteppingWheel.cpp
 *
 * method:
 * ライントレース向け
 *  driveInit: 走行を開始します。
 *  driveStop: 走行を停止します。
 *  setDistance: 車輪スピードを左右で変更します。引数: スピード差(右に進むなら正、左なら負)
 * 
 * 決め打ち制御向け
 *  driveAbs: 指定回数回します。第二引数で速度の変更ができます。
 *  turn: 指定回数回します。第二引数で速度の変更、第三引数で回転方向を設定ができます。
 * 
 */

#include "Arduino.h"
#include "SteppingWheel.h"
#include <TimerOne.h>
#include <MsTimer2.h>
#include <TimerThree.h>

#define CLKWISE 1 // 車輪右回転
#define COUNTERCLKWISE -1 // 車輪左回転

SteppingWheel::SteppingWheel(void (*leftfunc)(int), void (*rightfunc)(int), unsigned long maxspeed, unsigned long minspeed) {
  left = leftfunc;
  right = rightfunc;
  basespeed = maxspeed;
  lowspeed = minspeed;
}

void SteppingWheel::driveInit() {
  Timer1.initialize(lowspeed);
  Timer3.initialize(lowspeed);
  Timer1.attachInterrupt(&SteppingWheel::driveL);
  Timer3.attachInterrupt(&SteppingWheel::driveR);
  MsTimer2::set(1, &SteppingWheel::speedUp);
  MsTimer2::start();
}

void SteppingWheel::driveStop() {
  MsTimer2::set(1, &SteppingWheel::speedDown);
  MsTimer2::start();
}

void SteppingWheel::setDistance(int speed) {
  leftspeed = basespeed;
  rightspeed = basespeed;
  if(speed > 0) {
    leftspeed -= speed;
  } else if(speed < 0) {
    rightspeed += speed;
  }
}

void SteppingWheel::driveAbs(int count, boolean isMaxSpeed) {
  int spd;
  if(isMaxSpeed) {
    spd = basespeed;
  } else {
    spd = lowspeed;
  }

  for (int i = 0; i < count; i++){
    left(CLKWISE);
    right(COUNTERCLKWISE);
    delayMicroseconds(spd);
  }
}

void SteppingWheel::turn(int count, boolean isMaxSpeed, boolean isLeft) {
  int spd;
  if(isMaxSpeed) {
    spd = basespeed;
  } else {
    spd = lowspeed;
  }

  if(isLeft) {
    for (int i = 0; i < count; i++){
      left(COUNTERCLKWISE);
      right(COUNTERCLKWISE);
      delayMicroseconds(spd);
    }
  } else {
    for (int i = 0; i < count; i++){
      left(CLKWISE);
      right(CLKWISE);
      delayMicroseconds(spd);
    }
  }
}

void SteppingWheel::driveL() {
  left(CLKWISE);
  Timer1.setPeriod(leftspeed);
}

void SteppingWheel::driveR() {
  right(COUNTERCLKWISE);
  Timer3.setPeriod(rightspeed);
}

void SteppingWheel::speedUp() {
  static int speed = lowspeed;
  if(speed > basespeed) {
    speed -= 10000;
    leftspeed = speed;
    rightspeed = speed;
  } else {
    leftspeed = basespeed;
    rightspeed = basespeed;
    MsTimer2::stop();
  }
}

void SteppingWheel::speedDown() {
  static int speed = basespeed;
  if(speed < lowspeed) {
    speed += 10000;
    leftspeed = speed;
    rightspeed = speed;
  } else {
    Timer1.stop();
    Timer3.stop();
    MsTimer2::stop();
  }
}