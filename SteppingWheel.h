#ifndef SteppingWheel_h
#define SteppingWheel_h

class SteppingWheel{
  public:
  /* コンストラクタ */
  SteppingWheel(void (*leftfunc)(int), void (*rightfunc)(int), unsigned long maxspeed, unsigned long minspeed);

  /* ライントレース用関数類 */
  void driveInit(); // 前進開始
  void driveStop(); // 前進終了
  void setDistance(int speed); // 左右の車輪スピード差セット(右気味に:正 左気味に:負)

  /* 決め打ち制御用関数類 */
  void driveAbs(int count, boolean isMaxSpeed);   // 前進(spd: 1角動かすスピード, count: 角の数)
  void turn(int count, boolean isMaxSpeed, boolean isLeft);   // 回転(d = true: 右回転 false: 左回転)

  private:
  static void (*left)(int), (*right)(int);  // ステップ関数
  static void driveL(), driveR(), speedUp(), speedDown();

  static unsigned long basespeed, lowspeed, leftspeed, rightspeed;
};

#endif