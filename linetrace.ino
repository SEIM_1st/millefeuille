/*
 * linetrace.ino
 * 
 * function:
 * 
 */

/* Arduinoのピン定義 */
const int prlout = A10;   // 左外側フォトリフレクタ
const int prlin = A13;  // 左内側
const int prrin = A12;     // 右内側
const int prrout = A11;   // 右外側

/* 目標値 */
#define TARGET 0

/* 閾値 */
#define BLACK 30 // 黒と判定する閾値

/* PID制御用パラメータ */
#define KP 0
#define KI 0
#define KD 0

void linetraceSetup() {
  pinMode(prlout, INPUT);
  pinMode(prlin, INPUT);
  pinMode(prrin, INPUT);
  pinMode(prrout, INPUT);
}

void linetrace(int stopCount) {
  int blackLine = 0;
  int diff, speed;
  while (blackLine < stopCount) {
    if (analogRead(prlout) < BLACK && analogRead(prrout) < BLACK) {
      blackLine++;
    }
    diff = readSensor() - TARGET;
    speed = pid(diff);
    wheel.setDistance(speed);
  }
}

int readSensor() {
  return analogRead(prlout) + analogRead(prlin) - analogRead(prrin) - analogRead(prrout);
}

int pid(int diff) {
    static int lastdiff = diff;
    
    lastdiff = diff;
    
}