/*
 * color_sensor.ino
 * 
 * function:
 * void colorsetup(): ピンモードを設定します。void setup()時に使用してください。
 * int checkcolor(): 色を返します。もし明度が高い場合、-1を返します。
 * 
 */

/* Arduinoのピン定義 */
const int doutPin = 10;   // Dout
const int rangePin = 13;  // Range
const int ckPin = 12;     // CK
const int gatePin = 11;   // Gate

/* 明度の閾値 */
#define MAX_VALUE 1700

/* 色の定義 */
#define NO_BALL -1
#define RED 0
#define BLUE 1
#define YELLOW 2

/* 色の閾値 */
#define RED_THRESHOLD 30     //赤と青の閾値
#define BLUE_THRESHOLD 120   //青と黄の閾値
#define YELLOW_THRESHOLD 240 //黄と赤の閾値

void colorSetup() {
  // ピンモードを設定する。doutPinは入力、それ以外は出力。
  pinMode(doutPin, INPUT);
  pinMode(rangePin, OUTPUT);
  pinMode(ckPin, OUTPUT);
  pinMode(gatePin, OUTPUT);
}

int checkColor() {
  int red, green, blue;  // 測定した値を格納するための変数
  int integration = 50;  // 測定時間(ミリ秒)を格納する変数
  float hue;             // 色相

  digitalWrite(gatePin, LOW);        // GateとCKをLOWにする。
  digitalWrite(ckPin, LOW);
  digitalWrite(rangePin, HIGH);      // RangeをHIGHにする。

  digitalWrite(gatePin, HIGH);       // GateをHIGHにして測定開始。
  delay(integration);                // 測定時間だけ待つ。
  digitalWrite(gatePin, LOW);        // GateをLOWにして測定終了。
  delayMicroseconds(4);              // 4ミリ秒待つ。
  red = shiftIn12(doutPin, ckPin);   // 赤色の値を読む。
  delayMicroseconds(3);              // 3ミリ秒待つ。
  green = shiftIn12(doutPin, ckPin); // 緑色の値を読む。
  delayMicroseconds(3);              // 3ミリ秒待つ。
  blue = shiftIn12(doutPin, ckPin);  // 青色の値を読む。

  if (isOverValue(red, green, blue)) return NO_BALL;                      // 明度の閾値より大きい場合、ボールなし判定をする

  hue = rgb2hue(red, green, blue);
  if (RED_THRESHOLD < hue && hue < BLUE_THRESHOLD) return YELLOW;           // 赤の閾値以上青の閾値以下の場合、青
  else if (BLUE_THRESHOLD < hue && hue < YELLOW_THRESHOLD) return BLUE; // 青の閾値以上黄の閾値以下の場合、黄
  else return RED;                                                        // それ以外の場合、赤
}

/* RGBを色相に変換する関数 */
float rgb2hue(int r, int g, int b) {
  int max = r > g ? r : g;
  max = max > b ? max : b;
  int min = r < g ? r : g;
  min = min < b ? min : b;
  float hue = max - min;
  if (hue > 0) {
    if (max == r) {
      hue = (float)(g - b) / hue;
    } else if (max == g) {
      hue = 2.0f + (float)(b - r) / hue;
    } else {
      hue = 4.0f + (float)(r - g) / hue;
    }
    hue *= 60;
    if (hue < 0) {
      hue += 360;
    }
  }

  return hue;
}

/* 明度が閾値以上か確認する関数 */
boolean isOverValue(int r, int g, int b) {
  int max = r > g ? r : g;
  max = max > b ? max : b;

  if (max > MAX_VALUE) return true;
  return false;
}

/* 12ビットの値を読み込む関数(LSBから送信されるデータを想定) */
int shiftIn12(int dataPin, int clockPin) {
  int value = 0;

  for (int i = 0; i < 12; i++) {
    digitalWrite(clockPin, HIGH);           // クロックをHIGHにする
    value |= digitalRead(dataPin) << i;     // データピンの値を読み取り、所定のビットを設定する。
    digitalWrite(clockPin, LOW);            // クロックピンをLOWにする。
  }

  return value;
}