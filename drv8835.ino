/*
 * drv8835.ino
 * 
 * function:
 * enableStep(boolean left, boolean right): 車輪回転を有効/無効切り替えます
 * step_L(int d): 左車輪を回転させます、d > 0で右回転、それ以外で左回転
 * step_R(int d): 右車輪
 */

#define AB 0 // A+B
#define lAB 1 // B+/A
#define lAlB 2 // /A+/B
#define AlB 3 // /B+A

const int AENBL_L;
const int APHASE_L;
const int BENBL_L;
const int BPHASE_L;

const int AENBL_R;
const int APHASE_R;
const int BENBL_R;
const int BPHASE_R;

void enableStep(boolean left, boolean right) {
    /* 左 */
    digitalWrite(AENBL_L, left);
    digitalWrite(BENBL_L, left);
    /* 右 */
    digitalWrite(AENBL_R, right);
    digitalWrite(BENBL_R, right);
}

void step_L(int d) {
  static int lastphase = AlB;
  lastphase = step(d, lastphase, APHASE_L, BPHASE_L);
}

void step_R(int d) {
  static int lastphase = AlB;
  lastphase = step(d, lastphase, APHASE_R, BPHASE_R);
}

int step(int d, int lastphase, int APHASE, int BPHASE) {

  if(d > 0){ /* 右回転　*/
    /* AB -> /AB -> /A/B -> A/B */
    switch(lastphase) {
      case AB:
        digitalWrite(BPHASE, HIGH);
        return lAB;
      case lAB:
        digitalWrite(APHASE, LOW);
        return lAlB;
      case lAlB:
        digitalWrite(BPHASE, LOW);
        return AlB;
      case AlB:
        digitalWrite(APHASE, HIGH);
        return AB;
    }
  } else { /* 左回転 */
    /* A/B -> /A/B -> /AB -> AB */
    switch(lastphase) {
      case AlB:
        digitalWrite(APHASE, LOW);
        return lAlB;
      case lAlB:
        digitalWrite(BPHASE, HIGH);
        return lAB;
      case lAB:
        digitalWrite(APHASE, HIGH);
        return AB;
      case AB:
        digitalWrite(BPHASE, LOW);
        return AlB;
    }
  }
}

