/*
 *  Millefeuille Main Program
 *  author: Kio Matsuda
 *  
 *  require:
 *  SteppingWheel.cpp
 *  SteppingWheel.h
 *  color_sensor.ino
 *  linetrace.ino
 *  distance.ino
 */
#include "millefeuille.h"

/* 基本スピード */
#define SPEED 50000
#define LOWSPEED 100000

/* ステッピングモーターのピン設定 */


/* ステッピングモーターのインスタンス作成 */
SteppingWheel wheel(step_L, step_R, SPEED, LOWSPEED);


/* ======================================================== */
/*  setup()                                                 */
/* ======================================================== */
void setup() {
  //colorSetup();

}

/* ======================================================== */
/*  loop()                                                  */
/* ======================================================== */
void loop() {
}

/* ================================= */
/*  メインの動き                      */
/* ================================= */
inline void begin() { /* スタートから自由ボール落としてラインに乗るまで */

}

inline void moveToArea() { /* ラインに乗ってからボールある場所前まで */

}

inline void storeBall() { /* ボールを見つけ取り格納するまで */

}

inline void moveToBasket() { /* ボールをかごにいれる */

}

